DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `new_procedure`(IN xcall_number INT,OUT result INT)
BEGIN

SELECT count(*) AS COUNT_OF_INCIDENT_INVOLVED
FROM call_entity NATURAL JOIN incident
WHERE call_number = xcall_number
INTO result;

END