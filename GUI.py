import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import mysql.connector

class Counter_program():
    connection=mysql.connector.connect(user='root', password='a493426943487Ek*', host='127.0.0.1', database='mydb');

    def __init__(self):
        self.window = tk.Tk()
        self.window.title("San Francisco Fire Database")
        self.create_widgets()

    def gettingCallNum(self, event=None):
        cursor = self.connection.cursor()
        SQLClause = "SELECT count(*) AS COUNT_OF_INCIDENT_INVOLVED FROM call_entity NATURAL JOIN incident WHERE call_number = %s" %self.CallNumber.get()
        cursor.execute(SQLClause)
        TK = [i for i in cursor]
        if TK[0][0] == 0:
            reply = self.label.set("no service call for the call number")
        else:
            reply = self.label.set("the call number has asked for help for %s time" % TK[0][0])
        return reply
    def gettingALS(self):
        cursor=self.connection.cursor()
        SQLClause="Select count(*),als_unit From incident Where als_unit=%s;"%self.ALSradio_variable.get()
        cursor.execute(SQLClause)
        TK=[i for i in cursor]
        return self.ResultListbox.insert(tk.END,TK[0][0])

    def ResponseTime(self, event=None):
        cursor=self.connection.cursor()
        ResponseT=self.MaxMinCombo.get()
        SQLClause=""
        if ResponseT=="Maximum":
            SQLClause="Select incident_number,call_number, timediff(entry_timestamp,received_timestamp) as difference1 From incident Order by difference1 desc Limit 1;"
        else:
            SQLClause="Select incident_number,call_number, timediff(entry_timestamp,received_timestamp) as difference1 From incident Order by difference1 asc Limit 1;"
        cursor.execute(SQLClause)
        TK=[i for i in cursor]
        return self.ResultListbox.insert(tk.END,TK[0][2])

    def gettingIncidentNumber(self, event=None):
        cursor=self.connection.cursor()
        SQLClause="select adress from incident where incident_number=%s;"%self.IncidentNumber.get();
        cursor.execute(SQLClause)
        TK= [i for i in cursor]
        return self.ResultListbox.insert(tk.END,"Incident Adress:",TK[0][0])

    def gettingCallType(self, event=None):
        cursor=self.connection.cursor()
        SQLClause="Select count(incident_number),call_type From incident join (call_entity join call_type on call_entity.call_type_number=call_type.call_type_number) where call_type='%s';"%self.comboCallType.get()
        cursor.execute(SQLClause)
        TK=[i for i in cursor]
        return self.ResultListbox.insert(tk.END,TK[0][1],TK[0][0])

    def create_widgets(self):
        self.window['padx'] = 100
        self.window['pady'] = 100
        data_frame = ttk.LabelFrame(self.window, text="Data Search", relief=tk.RIDGE)
        data_frame.grid(row=1, column=1, sticky=tk.E + tk.W + tk.N + tk.S)

        entry_label = ttk.Label(data_frame, text="ALS: ")
        entry_label.grid(row=1, column=1, sticky=tk.W)

        self.ALSradio_variable = tk.StringVar()
        self.ALSradio_variable.set("1")

        radioTrue = ttk.Radiobutton(data_frame, text="True",variable=self.ALSradio_variable, value="1")
        radioTrue.grid(row=1, column=2, sticky=tk.W)
        radioFalse = ttk.Radiobutton(data_frame, text="False",variable=self.ALSradio_variable, value="0")
        radioFalse.grid(row=1, column=2, sticky=tk.E)

        ALSBtn = tk.Button(data_frame, text="Find Data",command=self.gettingALS)
        ALSBtn.grid(row=1, column=3, sticky=tk.E)
        self.comboCallType = tk.StringVar()

        self.MaxMinCombo = tk.StringVar()
        MaxMinComboBox = ttk.Combobox(data_frame, height=2, textvariable=self.MaxMinCombo)
        MaxMinComboBox.grid(row=2, column=2)
        MaxMinComboBox['values'] = ("Maximum", "Minimum")
        MaxMinComboBox.current(0)

        responseLabel=ttk.Label(data_frame,text="Response Time: ")
        responseLabel.grid(row=2,column=1,sticky=tk.W)

        ResponseTimeBtn = tk.Button(data_frame, text="Find Data",command=self.ResponseTime)
        ResponseTimeBtn.grid(row=2, column=3, sticky=tk.E)

        callLabel=ttk.Label(data_frame,text="Call Number: ")
        callLabel.grid(row=3,column=1,sticky=tk.W)

        self.CallNumber=tk.StringVar()

        CallNumberTextbox=ttk.Entry(data_frame, width=40, textvariable=self.CallNumber)
        CallNumberTextbox.grid(row=3, column=2, pady=5)

        CallNumberBtn = tk.Button(data_frame, text="Find Data",command=self.gettingCallNum)
        CallNumberBtn.grid(row=3, column=3, sticky=tk.E)

        callLabel=ttk.Label(data_frame,text="Incident Number: ")
        callLabel.grid(row=4,column=1,sticky=tk.W)

        self.IncidentNumber=tk.StringVar()

        IncidentNumberTextbox=ttk.Entry(data_frame, width=40, textvariable=self.IncidentNumber)
        IncidentNumberTextbox.grid(row=4, column=2, pady=5)

        IncidentNumberBtn = tk.Button(data_frame, text="Find Data",command=self.gettingIncidentNumber)
        IncidentNumberBtn.grid(row=4, column=3, sticky=tk.E)

        callLabel=ttk.Label(data_frame,text="Call Type: ")
        callLabel.grid(row=5,column=1,sticky=tk.W)

        cursor=self.connection.cursor()
        SQL="Select count(incident_number),call_type From incident join (call_entity join call_type on call_entity.call_type_number=call_type.call_type_number) Group by call_type;"
        cursor.execute(SQL)
        callTypeLists=[i for i in cursor]
        callTypesList=[]
        for i in range(len(callTypeLists)):
            callTypesList.append(callTypeLists[i][1])
        print(callTypesList)

        CallTypes = ttk.Combobox(data_frame, height=4, textvariable=self.comboCallType)
        CallTypes.grid(row=5, column=2, sticky=tk.W)
        CallTypes['values'] = callTypesList
        CallTypes.current(0)

        CallTypeBtn = tk.Button(data_frame, text="Find Data",command=self.gettingCallType)
        CallTypeBtn.grid(row=5, column=3, sticky=tk.E)

        self.Listbox=tk.StringVar()
        self.ResultListbox=tk.Listbox(self.window)
        self.ResultListbox.grid(row=2, column=1,pady=3)

        self.label=tk.StringVar();
        myLabel=ttk.Label(self.window, text='', textvariable=self.label);
        myLabel.grid(row=3, column=1,pady=3);

        quit_button = ttk.Button(self.window, text="Quit", command=self.window.destroy)
        quit_button.grid(row=9, column=9)



# Create the entire GUI program
program = Counter_program()

# Start the GUI event loop
program.window.mainloop()
