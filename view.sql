CREATE VIEW units_of_support_type AS 
SELECT unit_id,unit_type
FROM unit NATURAL JOIN unit_type
WHERE unit_type = "SUPPORT"

