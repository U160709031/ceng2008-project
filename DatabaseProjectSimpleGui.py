import tkinter as tk
from tkinter import ttk
from tkinter import filedialog


class Counter_program():
    def __init__(self):
        self.window = tk.Tk()
        self.window.title("San Francisco Fire Database")
        self.create_widgets()

    def gettingALS(self, event=None):
        ALS=self.radio_variable.get();
        reply=self.label.set(format(ALS));
        return reply

    def gettingCallType(self, event=None):
        CType=self.comboCallType.get();
        reply=self.label.set(format(CType));
        return reply

    def gettingCallNum(self, event=None):
        CNum=self.CallNumber.get();
        reply=self.label.set(format(CNum));
        return reply

    def gettingIncidentNumber(self, event=None):
        INum=self.IncidentNumber.get();
        reply=self.label.set(format(INum));
        return reply

    def gettingData(self, event=None):
        dataS=self.entrybox_value.get();
        reply=self.label.set(format(dataS));
        return reply

    def create_widgets(self):
        # Create some room around all the internal frames
        self.window['padx'] = 100
        self.window['pady'] = 100
        self.entrybox_value=tk.StringVar()
      # The Commands frame
        # cmd_frame = ttk.LabelFrame(self.window, text="Commands", padx=5, pady=5, relief=tk.RIDGE)
        data_frame = ttk.LabelFrame(self.window, text="Data Search", relief=tk.RIDGE)
        data_frame.grid(row=1, column=1, sticky=tk.E + tk.W + tk.N + tk.S)


        entry_label = ttk.Label(data_frame, text="ALS: ")
        entry_label.grid(row=1, column=1, sticky=tk.W)

        self.radio_variable = tk.StringVar()
        self.radio_variable.set("0")

        radioTrue = ttk.Radiobutton(data_frame, text="True",variable=self.radio_variable, value="0")
        radioTrue.grid(row=1, column=2, sticky=tk.W)
        radioFalse = ttk.Radiobutton(data_frame, text="False",variable=self.radio_variable, value="1")
        radioFalse.grid(row=1, column=2, sticky=tk.E)

        ALSBtn = tk.Button(data_frame, text="Find Data",command=self.gettingALS)
        ALSBtn.grid(row=1, column=3, sticky=tk.E)
        self.window.bind("<Return>",self.gettingALS);
        ALSBtn.bind("<Button-1>",self.gettingALS);
        self.comboCallType = tk.StringVar()

        callLabel=ttk.Label(data_frame,text="Call Type: ")
        callLabel.grid(row=2,column=1,sticky=tk.W)

        CallTypes = ttk.Combobox(data_frame, height=4, textvariable=self.comboCallType)
        CallTypes.grid(row=2, column=2, sticky=tk.W)
        CallTypes['values'] = ("Choice one", "Choice two", "Choice three", "Choice four")
        CallTypes.current(0)

        CallTypeBtn = tk.Button(data_frame, text="Find Data",command=self.gettingCallType)
        CallTypeBtn.grid(row=2, column=3, sticky=tk.E)
        self.window.bind("<Return>",self.gettingCallType);
        CallTypeBtn.bind("<Button-1>",self.gettingCallType);

        callLabel=ttk.Label(data_frame,text="Call Number: ")
        callLabel.grid(row=3,column=1,sticky=tk.W)

        self.CallNumber=tk.StringVar()

        CallNumberTextbox=ttk.Entry(data_frame, width=40, textvariable=self.CallNumber)
        CallNumberTextbox.grid(row=3, column=2, pady=5)

        CallNumberBtn = tk.Button(data_frame, text="Find Data",command=self.gettingCallNum)
        CallNumberBtn.grid(row=3, column=3, sticky=tk.E)
        self.window.bind("<Return>",self.gettingCallNum);
        CallNumberBtn.bind("<Button-1>",self.gettingCallNum);

        callLabel=ttk.Label(data_frame,text="Incident Number: ")
        callLabel.grid(row=4,column=1,sticky=tk.W)

        self.IncidentNumber=tk.StringVar()

        IncidentNumberTextbox=ttk.Entry(data_frame, width=40, textvariable=self.IncidentNumber)
        IncidentNumberTextbox.grid(row=4, column=2, pady=5)

        IncidentNumberBtn = tk.Button(data_frame, text="Find Data",command=self.gettingIncidentNumber)
        IncidentNumberBtn.grid(row=4, column=3, sticky=tk.E)
        self.window.bind("<Return>",self.gettingIncidentNumber);
        IncidentNumberBtn.bind("<Button-1>",self.gettingIncidentNumber);

        button_label = ttk.Label(data_frame, text="Search: ")
        button_label.grid(row=5, column=1, sticky=tk.W, pady=3)

        my_Textbox=ttk.Entry(data_frame, width=40, textvariable=self.entrybox_value)
        my_Textbox.grid(row=5, column=2, sticky=tk.W, pady=5)


        my_button = tk.Button(data_frame, text="Find Data",command=self.gettingData)
        my_button.grid(row=5, column=3, sticky=tk.E)
        self.window.bind("<Return>",self.gettingData);
        my_button.bind("<Button-1>",self.gettingData);

        self.label=tk.StringVar();
        myLabel=ttk.Label(self.window, text='', textvariable=self.label);
        myLabel.grid(row=2, column=1,pady=3);

        quit_button = ttk.Button(self.window, text="Quit", command=self.window.destroy)
        quit_button.grid(row=9, column=9)



# Create the entire GUI program
program = Counter_program()

# Start the GUI event loop
program.window.mainloop()