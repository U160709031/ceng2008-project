from bq_helper import BigQueryHelper
from pandas import DataFrame

bq_assistant = BigQueryHelper(active_project="bigquery-public-data",dataset_name="san_francisco")



# the following four object is constructed by taking relevant columns according to entity tables.

unit_entity = bq_assistant.head('sffd_service_calls',num_rows=3056).take([2,27,28],axis=1)
incident_entity = bq_assistant.head('sffd_service_calls',num_rows=3056).take([2,1,0,15,16,17,20,31,32,34,24,13,8,9,10,11,6,18,19,21,22,23,7],axis=1)
call_entity =bq_assistant.head('sffd_service_calls',num_rows=3056).take([0,4,3],axis=1)
call_type_entity = bq_assistant.head('sffd_service_calls',num_rows=3056).take([3],axis=1)

# to list unique call and unit types along whole table.

query_for_unique_call_types="""SELECT DISTINCT call_type FROM `bigquery-public-data.san_francisco.sffd_service_calls`"""
query_for_unique_unit_types="""SELECT DISTINCT unit_type FROM `bigquery-public-data.san_francisco.sffd_service_calls`"""

unit_types=bq_assistant.query_to_pandas_safe(query_for_unique_unit_types).head(10)
call_types=bq_assistant.query_to_pandas_safe(query_for_unique_call_types).head(50)

#to index the values of unit and call types and replace types with their indexes.

repr_unit_types=unit_types['unit_type'].tolist()
index_of_unit_types = [i for i in range(len(repr_unit_types))]
modified_unit_entity = unit_entity.replace(to_replace=repr_unit_types,value=index_of_unit_types)

repr_call_types=call_types['call_type'].tolist()
index_of_call_types = [i for i in range(len(repr_call_types))]
modified_call_entity = call_entity.replace(to_replace=repr_call_types,value=index_of_call_types)


#to format object as a csv file and write it into file.
with open('unit_entity.csv','w') as f:
    for item in modified_unit_entity.to_csv(index=False):
        f.write('%s' % item)
with open('incident_entity.csv','w') as f:
    for item in incident_entity.to_csv(index=False):
        f.write('%s' % item)
with open('call_entity.csv','w') as f:
    for item in modified_call_entity.to_csv(index=False):
        f.write('%s' % item)
with open('call_type_entity.csv','w') as f:
    for item in call_types.to_csv():
        f.write('%s' % item)
with open('unit_type_entity.csv','w') as f:
    for item in unit_types.to_csv():
        f.write('%s' % item)



