Select incident.incident_number,incident.call_number, timediff(on_scene_timestamp, entry_timestamp) as difference1
From incident join unit2incedent on incident.incident_number=unit2incedent.incident_number
Order by difference1 desc
Limit 1;

Select incident.incident_number,incident.call_number, timediff(on_scene_timestamp, entry_timestamp) as difference1
From incident join unit2incedent on incident.incident_number=unit2incedent.incident_number
Order by difference1 asc
Limit 1;

Select count(*)
From incident
Where incident_number in
(Select incident_number
From  call_entity join call_type on call_entity.call_type_number=call_type.call_type_number
Where call_type = "Medical Incident" and incident_number in
(Select incident_number
From  call_entity join call_Type on call_entity.call_type_number=call_type.call_type_number
Where call_type="Structure Fire"));

Select count(incident_number) as call_type_count,call_type
From incident join (call_type join call_entity on call_type.call_type_number=call_entity.call_type_number)
group by call_type
Order by call_type_count desc
Limit 5;

Select count(*),priority
From incident
Where priority=3;

Select incident_number,call_number, timediff(entry_timestamp,received_timestamp) as difference1
From incident
Order by difference1 desc
Limit 1;

Select incident_number,call_number, timediff(entry_timestamp,received_timestamp) as difference1
From incident
Order by difference1 asc
Limit 1;

Select count(*),als_unit
From incident
Where als_unit=true;

Select count(incident_number),call_type
From incident join (call_entity join call_type on call_entity.call_type_number=call_type.call_type_number)
Group by call_type;

Select count(incident_number),call_type
From incident join (call_entity join call_type on call_entity.call_type_number=call_type.call_type_number)
where call_type="Assist Police";

set @unitType="SUPPORT";
set @unitIDs=0;
call unitTypeswithID(@unitType,@unitIDs);
select @unitIDs;

show variables like "secure_file_priv";
